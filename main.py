import time
import tweepy
import requests
from bs4 import BeautifulSoup, CData

FCPS_TWITTER = "https://mobile.twitter.com/fcpsnews"
FCPS_EMERGENCY_PAGE = "https://www.fcps.edu/alert_msg_feed"

try:
    from secret import CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, \
        ACCESS_TOKEN_SECRET
except ImportError:
    print("You do not have a secret.py file")
    exit(1)


def twitterfeed():
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)
    tweets = api.user_timeline(screen_name = "@fcpsnews", count=20)
    for tweet in tweets:
        if tweet._json["text"] and "All Fairfax County public schools" in tweet._json["text"]:
            print(tweet._json["text"])

def fcpspage():
     r = requests.get("{}?{}".format(FCPS_EMERGENCY_PAGE, int(time.time() // 60)))
     print(r.text)
     print(r.url)


def welcome():
    print("Welcome to schoolsout - a smart utility to determine whether FCPS is closed or not.\n")
    print("Developed by Theo Ouzhinski <touzhinski@gmail.com>.  This project is licensed under the\n")
    print("MIT License, a copy of which is located in the source directory. \n")
    print("THE AUTHOR PROVIDES NO WARRANTY WHATSOEVER AND THE PROJECT IS RELEASED AS IS.\n")

welcome()
twitterfeed()
fcpspage()
